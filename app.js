var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');

//Extraia configuracao de acordo com o ambiente
var knexConfig = require('./knexfile')[process.env.NODE_ENV];
//Configure knex
var knex = require('knex')(knexConfig);
//Configure objection com a instancia configurada do knex
var {Model} = require('objection');
Model.knex(knex);
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

module.exports = app;
