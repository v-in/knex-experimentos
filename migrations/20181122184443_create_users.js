exports.up = function(knex, Promise) {
  return knex.schema.createTable('usuarios', table => {
    table.increments('id').primary();
    table.string('nome_usuario').notNull();
    table.string('email').notNull();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('usuarios');
};
