exports.up = function(knex, Promise) {
  return knex.schema.createTable('amizades', table => {
    table.increments('id').primary();
    table.integer('pessoa_a').notNull();
    table.integer('pessoa_b').notNull();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('amizades');
};
