exports.up = function(knex, Promise) {
  return knex.schema.alterTable('amizades', table => {
    table.renameColumn('pessoa_a', 'usuario_a');
    table.renameColumn('pessoa_b', 'usuario_b');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('amizades', table => {
    table.renameColumn('usuario_a', 'pessoa_a');
    table.renameColumn('usuario_b', 'pessoa_b');
  });
};
