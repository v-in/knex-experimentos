exports.up = function(knex, Promise) {
  return knex.schema.alterTable('usuarios', table => {
    table.unique('email');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('usuarios', table => {
    table.dropUnique('email');
  });
};
