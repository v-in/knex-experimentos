var {Model} = require('objection');

class Amizade extends Model {
  static get tableName() {
    return 'amizades';
  }
}

module.exports = Amizade;
