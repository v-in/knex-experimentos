const {Model} = require('objection');

class Usuario extends Model {
  static get tableName() {
    return 'usuarios';
  }

  static get relationMappings() {
    return {
      //Um usuario tera um campo .amigos
      amigos: {
        //Que vem de uma relacao do tipo n-n
        relation: Model.ManyToManyRelation,
        //Cada amigo e tambem um usuario
        modelClass: __dirname + '/Usuario',
        join: {
          //O join sera feito desta tabela para ela mesma
          from: 'usuarios.id',
          to: 'usuarios.id',
          //Atravez da tabela de amizades
          through: {
            from: 'amizades.usuario_a',
            to: 'amizades.usuario_b',
          },
        },
      },
    };
  }
}

module.exports = Usuario;
