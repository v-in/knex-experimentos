var router = require('express').Router();
var Amizade = require('../models/Amizade');

router.get('/', async function(req, res) {
  try {
    const result = await Amizade.query();
    res.json({
      data: result,
    });
  } catch (e) {
    res.status(400).json(e);
  }
});

router.post('/', async function(req, res) {
  try {
    const {usuario_a, usuario_b} = req.body;
    if (!(usuario_a && usuario_b)) res.status(400).end();
    const result = await Amizade.query().insertGraph([
      {
        usuario_a,
        usuario_b,
      },
      {
        usuario_b,
        usuario_a,
      },
    ]);
    res.json(result);
  } catch (e) {
    res.status(400).json(e);
  }
});

module.exports = router;
