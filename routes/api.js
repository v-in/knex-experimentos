var router = require('express').Router();
var usuariosRouter = require('./usuarios');
var amizadesRouter = require('./amizades');

router.use('/usuarios', usuariosRouter);
router.use('/amizades', amizadesRouter);

module.exports = router;
