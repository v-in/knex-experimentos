var express = require('express');
var apiRouter = require('./api');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Hello world');
});

router.use('/api', apiRouter);

module.exports = router;
