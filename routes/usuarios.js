var router = require('express').Router();
var Usuario = require('../models/Usuario');

router.get('/', async function(req, res) {
  try {
    //Extraia filtros do query
    const {nome_usuario, email} = req.query;
    const usuarios = await Usuario.query()
      //Nao execute caso usuario nao tenha passado os argumentos esperados
      .skipUndefined()
      //Permita pre-carregamento de amigos e amigos de amigos
      .allowEager('[amigos, amigos.amigos]')
      .eager(req.query.eager)
      //Aplique filtros
      .where({nome_usuario, email});
    res.json({
      data: usuarios,
    });
  } catch (e) {
    console.log(e);
    res.status(400).send(e.toString());
  }
});

router.post('/', async function(req, res) {
  try {
    const result = await Usuario.query().insert(req.body);
    res.json(result);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
