exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('amizades')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('amizades').insert([
        {usuario_a: 1, usuario_b: 2},
        {usuario_a: 2, usuario_b: 1},
        {usuario_a: 1, usuario_b: 3},
        {usuario_a: 1, usuario_b: 3},
        {usuario_a: 2, usuario_b: 2},
        {usuario_a: 2, usuario_b: 3},
        {usuario_a: 3, usuario_b: 3},
        {usuario_a: 4, usuario_b: 3},
        {usuario_a: 5, usuario_b: 2},
        {usuario_a: 6, usuario_b: 2},
      ]);
    });
};
