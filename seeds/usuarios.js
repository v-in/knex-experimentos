exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('usuarios')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('usuarios').insert([
        {nome_usuario: 'vinicius', email: 'vinicius@icloud.com'},
        {nome_usuario: 'allan', email: 'allan@icloud.com'},
        {nome_usuario: 'luciano', email: 'luciano@icloud.com'},
        {nome_usuario: 'lucas', email: 'lucas@icloud.com'},
        {nome_usuario: 'allison', email: 'allison@icloud.com'},
        {nome_usuario: 'rhenan', email: 'rhenan@icloud.com'},
        {nome_usuario: 'gabriel', email: 'gabriel@icloud.com'},
      ]);
    });
};
